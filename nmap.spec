%global _hardened_build 1

Name:           nmap
Epoch:          2
Version:        7.94
Release:        7
License:        GPL-2.0-or-later
Summary:        A tool for network discovery and security auditing.
Requires:       %{name}-ncat = %{epoch}:%{version}-%{release}
URL:            https://nmap.org/
Source0:        https://nmap.org/dist/%{name}-%{version}.tar.bz2
BuildRequires:  automake autoconf gcc-c++ gettext-devel libpcap-devel libssh2-devel
BuildRequires:  libtool lua-devel openssl-devel pcre2-devel

Obsoletes:      nmap-frontend nmap-ndiff nmap-ncat nc < 1.109.20120711-2
Obsoletes:      nc6 < 1.00-22
Provides:       nmap-frontend nmap-ndiff nmap-ncat nc nc6

Patch0001:      backport-nmap-4.03-mktemp.patch
Patch0002:      backport-nmap_resolve_config.patch
Patch0003:      nmap-replace-sensitive-words.patch
Patch0004:      backport-upgrade-libpcre-to-PCRE2-10.42.patch
Patch0005:      backport-remove-nse_pcrelib-from-build.patch
Patch0006:      backport-nping-fix-out-of-bounds-access.patch
Patch0007:      backport-Ncat-server-UDP-do-not-quit-after-EOF-on-STDIN.-Fixe.patch
Patch0008:      remove-password-printing.patch
Patch0009:      backport-Bug-Lua-can-generate-wrong-code-when-_ENV-is-const.patch

%define pixmap_srcdir zenmap/share/pixmaps

%description
Nmap ("Network Mapper") is a free and open source (license) utility for network discovery and security \
auditing. It was designed to rapidly scan large networks, but works fine against single hosts.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

rm -rf libpcap libpcre macosx mswin32 libssh2 libz

%build
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
export CXXFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
%configure  --with-libpcap=yes --with-liblua=included --without-zenmap --without-ndiff  --enable-dbus --with-libssh2=yes
%make_build

sed -i 's/-md/-mf/' nping/docs/nping.1

%check
make check

%install
make DESTDIR=%{buildroot} STRIP=true install

rm -f %{buildroot}%{_datadir}/ncat/ca-bundle.crt
rmdir %{buildroot}%{_datadir}/ncat

ln -s ncat.1.gz %{buildroot}%{_mandir}/man1/nc.1.gz
ln -s ncat %{buildroot}%{_bindir}/nc

%find_lang nmap --with-man

%files -f nmap.lang
%defattr(-,root,root)
%doc ncat/docs/AUTHORS ncat/docs/README 
%license LICENSE
%{_bindir}/n*
%{_datadir}/nmap
%exclude %{_datadir}/ncat

%files help
%defattr(-,root,root)
%doc docs/README docs/nmap.usage.txt ncat/docs/THANKS ncat/docs/examples
%{_mandir}/man1/*.1.gz

%changelog
* Wed Jan 15 2025 xingwei <xingwei14@h-partners.com> - 2:7.94-7
- Type:CVE
- CVE:CVE-2022-28805
- SUG:NA
- DESC:backport lua upstream patch to fix CVE-2022-28805

* Fri Jun 14 2024 xinghe <xinghe2@h-partners.com> - 2:7.94-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix license

* Tue Jan 16 2024 xingwei <xingwei14@h-partners.com> - 2:7.94-5
- Type:bugfix
- CVE:
- SUG:NA
- DESC:remove password printing

* Fri Sep 15 2023 xingwei <xingwei14@h-partners.com> - 2:7.94-4
- Type:bugfix
- CVE:
- SUG:NA
- DESC:enable dt check

* Tue Sep 12 2023 xingwei <xingwei14@h-partners.com> - 2:7.94-3
- Type:bugfix
- CVE:
- SUG:NA
- DESC:Ncat server UDP: do not quit after EOF on STDIN

* Wed Aug 23 2023 xingwei <xingwei14@h-partners.com> - 2:7.94-2
- Type:bugfix
- CVE:
- SUG:NA
- DESC:nping:fix out of bounds

* Thu Jul 27 2023 xingwei <xingwei14@h-partners.com> - 2:7.94-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update nmap to 7.94

* Thu Jul 06 2023 xingwei <xingwei14@h-partners.com> - 2:7.92-5
- Type:bugfix
- CVE:
- SUG:NA
- DESC:add pcre2 support

* Mon May 29 2023 xingwei <xingwei14@h-partners.com> - 2:7.92-4
- Type:bugfix
- CVE:
- SUG:NA
- DESC:decouple from PCRE because it has been End-of-life, use local libpcre to build instead

* Fri Jun 17 2022 gaihuiying <eaglegai@163.com> - 2:7.92-3
- Type:bugfix
- CVE:
- SUG:NA
- DESC:replace sensitive words

* Tue Feb 08 2022 xinghe <xinghe2@h-partners.com> - 2:7.92-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix nc command permission error

* Thu Dec 09 2021 quanhongfei <quanhongfei@huawei.com> - 2:7.92-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update nmap to 7.92

* Sun Jun 28 2020 gaihuiying <gaihuiying@huawei.com> - 2:7.80-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:sync LTS branch to update nmap to 7.80

* Wed Jun 24 2020 gaihuiying <gaihuiying1@huawei.com> - 2:7.70-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete make check

* Sat Feb 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:7.70-11
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add make check

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:7.70-10
- Type:NA
- ID:NA
- SUG:NA
- DESC:delete unused patches

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:7.70-9
- Type:cves
- ID:CVE-2018-15173
- SUG:restart
- DESC:fix CVE-2018-15173

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:7.70-8
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:add 3rd-party licenses

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:7.70-7
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the location of COPYING

* Wed Sep 25 2019 Li Wang<wangli221@huawei.com> - 2:7.70-6
- Type:cves
- ID:CVE-2017-18594
- SUG:restart
- DESC:fix CVE-2017-18594

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:7.70-5
- Package init
